﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
    public float RotationSpeed = 100, MovementSpeed = 10;
    void Update()
    {
        //Links
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, -RotationSpeed * Time.deltaTime, 0);
        }
        //Rechts
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime, 0);
        }
        //Boven
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, -MovementSpeed * Time.deltaTime);
        }
        //Onder
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, MovementSpeed * Time.deltaTime);
        }
    }
}
