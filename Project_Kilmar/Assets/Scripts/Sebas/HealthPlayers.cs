﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthPlayers : MonoBehaviour
{
    public bool shielded = false;

    [SerializeField] private float m_maxHealt = 100f;
    [SerializeField] protected float m_currentHealth = 0f;
    [SerializeField] GameObject m_shield;

    [SerializeField] private GameObject m_explosion;

    [SerializeField] private Button Button1, Button2;

    DeathUI m_DeathUI;

    private void Start()
    {
        m_currentHealth = m_maxHealt;
        m_DeathUI = new DeathUI(Button1, Button2);
    }

    public void DoDamage(float damage)
    {
        m_currentHealth -= damage;
        shielded = false;
        if (m_currentHealth <= 0f)
        {
            Die();            
            m_DeathUI.SetButtonsActiveTrue();            
        }
    }

    private void Update()
    {
        if (shielded == true)
        {
            m_shield.SetActive(true);
        }
        if (shielded == false)
        {
            m_shield.SetActive(false);
        }
    }

    private void Die()
    {
        GameObject clone = Instantiate(m_explosion, transform.position, transform.rotation);        
        Destroy(gameObject);
    }   
}
