﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Shooting : MonoBehaviour
{
    RaycastHit RayHit;
    private float TheDamage = 25;

    [SerializeField]
    private ParticleSystem m_ParticleShoot;

    [SerializeField]
    private Transform OriginPlayer;

    void Start()
    {
        m_ParticleShoot.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            ShootRaycast();
            Debug.DrawLine(OriginPlayer.position, OriginPlayer.forward, Color.red);
            m_ParticleShoot.gameObject.SetActive(true);
            m_ParticleShoot.Play();
        }
    }

    private void ShootRaycast()
    {
        if (Physics.Raycast(OriginPlayer.position, -OriginPlayer.forward * 100f, out RayHit, 100f))
        {
            if (RayHit.transform.CompareTag("Player"))
            {
                Debug.Log(RayHit);
                RayHit.transform.gameObject.GetComponent<HealthPlayers>().DoDamage(TheDamage);
                if (GetComponent<SliderScript>())
                {
                    RayHit.transform.gameObject.GetComponent<SliderScript>().Player1Damage();
                }
            }
        }
    }
}
