﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gass : MonoBehaviour
{

    [SerializeField]
    private GameObject GassParticle;

	// Use this for initialization
	void Start () {
        GassParticle.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.S))
        {
            GassParticle.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            GassParticle.SetActive(false);
        }
    }
}
