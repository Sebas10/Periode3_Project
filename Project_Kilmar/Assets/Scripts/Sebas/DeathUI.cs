﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathUI : MonoBehaviour
{

    private Button m_RestartButton, m_BackToMenuButton;

    public DeathUI(Button Restart, Button BackToMenu)
    {
        m_RestartButton = Restart;
        m_BackToMenuButton = BackToMenu;
    }

    void Start()
    {
        
    }

    public void RestartOnClick()
    {
        SceneManager.LoadScene(1);
    }
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void SetButtonsActiveTrue()
    {
        m_RestartButton.gameObject.SetActive(true);
        m_BackToMenuButton.gameObject.SetActive(true);
    }
}
