﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnFlag : MonoBehaviour
{
    private int RaysToShoot = 10;
    RaycastHit RayHit;
    [SerializeField] private Transform Flag;

    private int numToSpawn = 1;
    private int spawned = 0;

    void Start()
    {
        Flag.position = new Vector3(Random.Range(20, -20f), 0.5f, Random.Range(20, -20));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Flag.position = new Vector3(Random.Range(20, -20f), 0.5f, Random.Range(20, -20));
        }
    }

    void FixedUpdate()
    {
        float angle = 1;
        for (int i = 0; i < RaysToShoot; i++)
        {
            float x = Mathf.Sin(angle);
            float y = Mathf.Cos(angle);
            angle += 2 * Mathf.PI / RaysToShoot;

            Vector3 dir = new Vector3(transform.position.x + x, transform.position.y + y, 0);
            RaycastHit hit;
            Debug.DrawLine(transform.position, dir, Color.red);
            if (Physics.Raycast(transform.position, dir, out hit))
            {
                if (hit.transform.CompareTag("Wall"))
                {
                    Flag.position = new Vector3(Random.Range(20, -20f), 0.5f, Random.Range(20, -20));
                }
            }
        }
    }
}
