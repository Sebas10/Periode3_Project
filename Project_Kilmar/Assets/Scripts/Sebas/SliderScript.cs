﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour {

    [SerializeField] private Slider m_SliderPlayer1, m_SliderPlayer2;

    private float m_Player1Health = 100;
    private float m_Player2Health = 100;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        m_SliderPlayer1.value = m_Player1Health;
        m_SliderPlayer2.value = m_Player2Health;
    }

    public void Player1Damage()
    {
        m_Player1Health -= 25f;
    }

    public void Player2Damage()
    {
        m_Player2Health -= 25f;
    }
}
