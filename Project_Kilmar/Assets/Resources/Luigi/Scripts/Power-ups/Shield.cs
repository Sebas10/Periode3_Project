﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private float m_Damage = 0;
    private float spawning = 4f;
    //Collision
    void OnTriggerEnter(Collider other)
    {
        //Check for players
        if (other.CompareTag("Player"))
        {
            StartCoroutine(Pickup(other.gameObject));
        }
    }
    IEnumerator Pickup(GameObject Player)
    {
        //Powering up
        if (Player.GetComponent<HealthPlayers>() && Player.GetComponent<HealthPlayers>().shielded == false)
        {
            Player.GetComponent<HealthPlayers>().DoDamage(m_Damage - 25);
            Player.GetComponent<HealthPlayers>().shielded = true;
        }
        //Disable Component
        GetComponent<Collider>().enabled = false;
        GetComponent<Transform>().position = new Vector3(Random.Range(20, -20f), 100, Random.Range(20, -20f));

        //Enable object
        yield return new WaitForSeconds(spawning);
        GetComponent<Collider>().enabled = true;
        GetComponent<Transform>().position = new Vector3(Random.Range(20, -20f), 0, Random.Range(20, -20f));
    }
}
