﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour
{
    public float multiplier;
    private float duration = 4f;
    private float spawning = 2f;
    //Collision
    void OnTriggerEnter(Collider other)
    {
        //Check for players
        if (other.CompareTag("Player"))
        {
            StartCoroutine( Pickup(other.gameObject));
        }
    }
    IEnumerator Pickup(GameObject Player)
    {
        //Powering up
        if (Player.GetComponent<MovementPlayer>())
        {
            Player.GetComponent<MovementPlayer>().MovementSpeed += multiplier;
        }
        else if (Player.GetComponent<MovementPlayer2>())
        {
            Player.GetComponent<MovementPlayer2>().MovementSpeed += multiplier;
        }
        //Disable Component
        GetComponent<Collider>().enabled = false;
        GetComponent<Transform>().position = new Vector3(Random.Range(20, -20f), 100, Random.Range(20, -20f));
        //Return to normal
        yield return new WaitForSeconds(duration);
        if (Player.GetComponent<MovementPlayer>())
        {
            Player.GetComponent<MovementPlayer>().MovementSpeed -= multiplier;
        }
        else if (Player.GetComponent<MovementPlayer2>())
        {
            Player.GetComponent<MovementPlayer2>().MovementSpeed -= multiplier;
        }
        //Enable object
        yield return new WaitForSeconds(spawning);
        GetComponent<Collider>().enabled = true;
        GetComponent<Transform>().position = new Vector3(Random.Range(20, -20f), 0, Random.Range(20, -20f));
    }
}
