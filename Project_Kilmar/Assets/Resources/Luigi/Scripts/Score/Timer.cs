﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField]
    public float m_Time;
    [SerializeField]
    private Text m_Text;

    void Start ()
    {
	}
	void Update ()
    {
        if (m_Time >= 0)
        {
            m_Text.text = m_Time.ToString();
            m_Time -= 1 * Time.deltaTime;
        }
        if (m_Time <= 0)
        {
            
        }
	}
}
