﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private bool CanCount = true;
    //UI
    [SerializeField]
    private GameObject m_DirectionalLight;
    [SerializeField]
    private GameObject m_ResultUI;
    //time
    [SerializeField]
    private float m_Time;
    [SerializeField]
    private Text m_Timer;
    [SerializeField]
    private GameObject m_timed;
    //score points
    [SerializeField]
    private float m_pointsplayer1;
    [SerializeField]
    private float m_pointsplayer2;
    //score text
    [SerializeField]
    private Text m_Player1score;
    [SerializeField]
    private Text m_Player2score;

    HealthPlayers m_playerhealth;
    

    private void Awake()
    {
        DontDestroyOnLoad(m_Player1score);
        DontDestroyOnLoad(m_Player2score);
        m_playerhealth = GetComponent<HealthPlayers>();
    }
    void Start ()
    {
    }
	void FixedUpdate ()
    {
        //timer
        if (m_Time >= 0 && CanCount == true)
        {
            //timer
            m_Timer.text = System.Math.Round(m_Time,0) + "";
            m_Time -= 1 * Time.deltaTime;
            //score
            m_Player1score.text = m_pointsplayer1.ToString();
            m_Player2score.text = m_pointsplayer2.ToString();
        }

        //Score
        GetComponent<SliderScript>().Player1Damage();
        GetComponent<SliderScript>().Player2Damage();

        //wrapping up
        if (m_Time < 0f)
        {
            if (m_pointsplayer1 > m_pointsplayer2)
            {
                //Player1 wins
                Debug.Log("player1 wins");
                m_Time = 0f;
                CanCount = false;
            }

            else if (m_pointsplayer2 > m_pointsplayer1)
            {
                //Player2 wins
                Debug.Log("player2 wins");
                m_Time = 0f;
                CanCount = false;
            }
            else
            {
                //it's a tie
                Debug.Log("It's a tie");
                m_Time = 0f;
                CanCount = false;
            }
        }
    }
}
