﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Restart : MonoBehaviour
{
	// Use this for initialization
	private void Start ()
    {	
	}
	// Update is called once per frame
	private void Update ()
    {
        if (Input.GetKey(KeyCode.R))
        {
            Restarter();
        }
    }
    void Restarter()
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
    }
}
